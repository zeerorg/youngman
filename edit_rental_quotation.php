<?php include("includes/header.php");
include("secure/db_connect.php");
$s_no = $_GET['id'];

$sql = "SELECT `row_total`, `sub_total`, `freight`, `tax`, `swach_bharat`, `kkc`, `total`, `createdby`, `customer_id`, `customer_name`, `delivery_address`, `delivery_date`, `pickup_date`, `delivery_pincode`, `site_name`, `contact_name`, `security_amt` FROM `table_quotation` WHERE s_no = ?";
//if($info = $mysqli->prepare($sql)){
$info = $mysqli->prepare($sql);
$info->bind_param('s', $s_no);
$info->execute();
$info->store_result();
$info->bind_result( $row_total, $sub_total, $freight, $tax, $swach_bharat, $kkc, $total, $created_by, $customer_id, $customer_name, $delivery_address, $delivery_date, $pickup_date, $pincode, $site_name, $contact_name, $security_amt);
$info->fetch();
//}else echo "prepare failed".$mysqli->error; 
 $table_data = '';
$stmt =  $mysqli->prepare("SELECT `type`, `item_code`, `desc`, `unit_price`, `qty`, `tot`  FROM table_quotation_item WHERE s_no = ?");
if($stmt){
    $stmt->bind_param('s',  $s_no); 
                   $stmt->execute(); // Execute the prepared query.
                   $stmt->store_result();
                   $stmt->bind_result($type, $item_code, $desc, $unit_price, $qty, $tot); // get variables from result

    $si = 0;
  
    while($stmt->fetch()) {
    $si++;        
        $table_data = $table_data.'
        <tr>
        <td><input class="case" type="checkbox"/></td>
        <td  style="display:none;"><select id="type_'.$si.'" name="item_type[]" class="itemType"><option value="Item">Item</option><option value="Bundle">Bundle</option></select></td>
        <td><input type="text" data-type="productCode" name="itemNo[]" id="itemNo_'.$si.'" class="form-control autocomplete_txt" autocomplete="off"  value="'.$item_code.'"></td>
        <td><input type="text" data-type="productName" name="itemName[]" id="itemName_'.$si.'" class="form-control autocomplete_txt" autocomplete="off" value="'.$desc.'"></td>
        <td><input type="number" name="price[]" id="price_'.$si.'" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="'.$unit_price.'"></td>
        <td><input type="number" name="quantity[]" id="quantity_'.$si.'" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="'.$qty.'"></td>
        <td><input type="number" name="total[]" id="total_'.$si.'" class="form-control totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="'.$tot.'"></td>
        </tr>
        ';
    }
    
}else echo "prepare failed".$mysqli->error;

?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
      <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
      <script>
$(document).ready(function(){
	 $("#name").keyup(function(){
		$.ajax({
		type: "POST",
		url: "readCustomerFromCache.php",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#name").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#name").css("background","#FFF");
             console.log("data");
		}
		});
	});
});

function selectCustomer(id,name) {
    console.log(name);
    $("#id").val(id);
    $("#suggesstion-box").hide();
    $("#name").val(name);  
}
</script>

      
      
    <section class="content-header">
      <h1>
        Edit Quotation
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Quotation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
	      <form role="form" method="post" action="secure/sec_quotation_add.php" enctype="multipart/form-data">
         <input type="hidden" name="csrf" value="<?php echo $_SESSION['login_string']; ?>" >
              <div class="box-body">
                             <div class="row">
               <div class="col-md-6">
                                  <div class="form-group">
                  <label for="id">Customer ID</label>
                  <input type="text" class="form-control" name="id" id="id" placeholder="Enter ID" value="<?php echo $customer_id; ?>" readonly>
                </div>
                  <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="<?php echo $customer_name; ?>">
                     
                </div>
                                  <div id="suggesstion-box"></div>
                    
                      <div class="form-group">
                                            <label for="del_date">Delivery Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="del_date" class="form-control pull-right" id="del_date" value="<?php echo $delivery_date; ?>">
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label for="site_name">Site Name</label>

                                            <input type="text" class="form-control" name="site_name" id="site_name" placeholder="Site Name" value="<?php echo $site_name ?>" required>


                                        </div>
                     <div class="form-group">
                                            <label for="contact_name">Contact Name</label>

                                            <input type="text" class="form-control" name="contact_name" id="contact_name" placeholder="Contact Name" value="<?php echo $contact_name ?>" required>


                                        </div>
                   
                   
                                 </div>
                  <div class="col-md-6">
                                 <div class="form-group">
                   <label for="delivery_address">Delivery Address</label>
                                    
                    <input type="text" class="form-control" name="delivery_address" id="delivery_address" placeholder="Delivery Address" value="<?php echo $delivery_address; ?>">   
                          
                </div>
                       <div class="form-group">
                                            <label for="delivery_address">Pin Code</label>

                                            <input type="text" class="form-control" name="deliveryAddPin" id="deliveryAddPin" placeholder="Enter Pincode" autocomplete="off" value="<?php echo $pincode; ?>" required>

                                        </div>
                      
                        <div class="form-group">
                                            <label for="pic_date">Pickup Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="pic_date" class="form-control pull-right" id="pic_date" value="<?php echo $delivery_date; ?>">
                                            </div>
                                        </div>
                      
                       <div class="form-group">
                                            <label>Billing Period</label>
                                            <select class="form-control" id="unit_dur" name="unit_dur">
                                                <option value="Days">Days</option>
                                                <option value="Months">Months</option>
                                               
                                            </select>
                                        </div>
                      <div class="form-group">
                                            <label for="security_amt">Security Amount</label>

                                            <input type="text" class="form-control" name="security_amt" id="security_amt" placeholder="Security Amt" value="<?php echo $security_amt; ?>"required>


                                        </div>
                 
	     </div>
                                 </div>
                  <hr>
                  
                     <div class='row'>
						      		<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
						      			<table class="table table-bordered table-hover" id="table_auto">
											<thead>
												<tr>
													<th width="2%"><input id="check_all" class="formcontrol" type="checkbox"/></th>
                                                    <th style="display:none;">Type</th>
													<th width="10%">Item No</th>
													<th>Description</th>
													<th width="10%">Unit Price</th>
													
                                                     <th width="10%">Duration</th>
                                                    
													<th width="10%">Total</th>
												</tr>
											</thead>
											<tbody>
												 <?php echo $table_data; ?>
											</tbody>
										</table>
						      		</div>
						      	</div>
						      	
						      	<div class='row'>
						      		<div class='col-xs-12 col-sm-3 col-md-3 col-lg-3'>
						      			<button class="btn btn-danger delete" type="button">- Delete</button>
						      			<button class="btn btn-success addmore" type="button">+ Add More</button>
						      		</div>
						      		
										<div class="col-md-6" style="float:right;">
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-addon"> Total: ₹</div>
													<input type="number" step="any" class="form-control" name="subTotal" id="subTotal" placeholder="Subtotal" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
												</div>
											</div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        Freight: ₹
                                                    </div>
                                                    <input type="number" step="any" class="form-control" name="freight" id="freight" placeholder="Freight" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $freight; ?>">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                 <div class="input-group">
                                                     <div class="input-group-addon">
                                                        Sub Total: ₹
                                                     </div>
                                                     <input type="number" step="any" class="form-control" name="sub_total_freight" id="sub_total_freight" placeholder="SubTotal" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $sub_total; ?>">
                                                 </div>
                                            </div>
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-addon">Tax: ₹</div>
													<input type="number" step="any" class="form-control" id="tax" name="tax" placeholder="Tax" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $tax; ?>">
												</div>
											</div>
                                             <div class="form-group">
                                                 <div class="input-group">
                                                    <div class="input-group-addon">
                                                        Swach Bharat: ₹
                                                     </div>
                                                     <input type="number" step="any"  class="form-control" name="swach_bharat" id="swach_bharat" placeholder="Swach Bharat" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $swach_bharat; ?>">
                                                 </div>
                                            </div>
                                             <div class="form-group">
                                                 <div class="input-group">
                                                    <div class="input-group-addon">
                                                        KKC: ₹
                                                     </div>
                                                     <input type="number" step="any" class="form-control" name="kkc" id="kkc" placeholder="KKC" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $kkc; ?>">
                                                 </div>
                                            </div>
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-addon">Total: ₹</div>
													<input type="number" step="any" class="form-control" name="totalAftertax" id="totalAftertax" placeholder="Total" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $total; ?>">
												</div>
											</div>
									</div>
						      	</div>
						      	
	   
              </div>
               
                  
                  
                  <hr>
                  
                  
                  
	   
	      <!-- /.box-body -->
	 <input  type="hidden" value="Rental" name="quot_type">
    <input type="hidden" value="<?php echo  $s_no; ?>" name="s_no">
	      <div class="box-footer">
	        <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
	      </div>
	    </form>
	    </div>
	   </div>
	  </div>
	 </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include("includes/footer.php"); ?>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="dist/js/jquery-ui.min.js"></script>
<script src="dist/js/rentalquot.js"></script>