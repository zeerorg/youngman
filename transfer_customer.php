<?php

include("secure/db_connect.php");
require_once dirname(__FILE__) . '/config.php';

$query = "SELECT name, company, customer_type, email, phone, mobile, website, billing_address_street, billing_address_city, billing_address_state, billing_address_pincode, billing_address_country, shipping_address_street, shipping_address_city, shipping_address_state, shipping_address_pin, shipping_address_country, opening_balance, opening_balance_date, tax_reg_no, credit_limit, payment_terms FROM temporary_customer";

if($Items  = $mysqli->prepare( $query )){
$Items ->execute();
$Items ->store_result();
$Items ->bind_result($name, $company, $customer_type, $email, $phone, $mobile, $website, $billing_address_street, $billing_address_city, $billing_address_state, $billing_address_pincode, $billing_address_country, $shipping_address_street, $shipping_address_city, $shipping_address_state, $shipping_address_pin, $shipping_address_country, $opening_balance, $opening_balance_date, $tax_reg_no, $credit_limit, $payment_terms);   
}else echo $mysqli->error;

while( $Items->fetch()){
    
    echo "<br>".$company." :";
    
            $CustomerService = new QuickBooks_IPP_Service_Customer();

            $Customer = new QuickBooks_IPP_Object_Customer();
            
            //$Customer->setGivenName($firstName);
            //$Customer->setLastName($lastName);
         
            $Customer->setDisplayName($company);
            $Customer->setCompanyName( $company);

            // Phone #
            $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
            $PrimaryPhone->setFreeFormNumber($phone);
            $Customer->setPrimaryPhone($PrimaryPhone);

            // Mobile #
            $Mobile = new QuickBooks_IPP_Object_Mobile();
            $Mobile->setFreeFormNumber($mobile);
            $Customer->setMobile($Mobile);


            // Bill address
            $BillAddr = new QuickBooks_IPP_Object_BillAddr();
            $BillAddr->setLine1($billing_address_street);
            $BillAddr->setLine2($billing_address_state);
         //   $BillAddr->setLine3($gst);
        //    $BillAddr->setLine4($cin);
            $BillAddr->setCity($billing_address_city);
          //  $BillAddr->setCountrySubDivisionCode('MI');
            $BillAddr->setPostalCode($billing_address_pincode);
            $Customer->setBillAddr($BillAddr);
         
         // Ship address supposed to be mailing address for sending bills
            $ShipAddr = new QuickBooks_IPP_Object_ShipAddr();
            $ShipAddr->setLine1($shipping_address_street);
            $ShipAddr->setLine2($mailAddLine2);
            $ShipAddr->setCity($shipping_address_city);
         //   $ShipAddr->setCountrySubDivisionCode('MI');
            $ShipAddr->setPostalCode($shipping_address_pin);
            $Customer->setShipAddr($ShipAddr);

            // Email
            $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
            $PrimaryEmailAddr->setAddress($email);
            $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);  
         
            $Customer->setBalance($opening_balance);
            $Customer->setOpenBalanceDate($opening_balance_date);

            if ($resp = $CustomerService->add($Context, $realm, $Customer))
            {
                 $id = QuickBooks_IPP_IDS::usableIDType($resp);
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);
                
                
                print(' Our new customer ID is: [' . $id . '] (name "' . $Customer->getDisplayName() . '")');
                //populate local db for cust local
               if ($insert_stmt = $mysqli->prepare("INSERT INTO `customer_local`    (customer_id ,category, mailing_address , mailing_pincode, credit_limit, security_letter, rental_advance, rental_order, security_check, credit_term, gst_no, cin_no) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"))
{
$insert_stmt->bind_param('ssssssssssss',  $id, $category, $mailing_address,  $mailAddPin, $creditLimit, $securityLetter, $rentalAdvance, $rentalOrder, $securityCheque, $creditTerm, $gst, $cin);
$insert_stmt->execute();
}
else echo $mysqli->error;
                
                
              //  header("Location: create_customer.php?success=".$resp);
            }
            else
            {
                print($CustomerService->lastError($Context));
                //Make toast tht it failed
               //  header("Location: create_customer.php?error=".$CustomerService->lastError($Context));
            }

    
    
                                                   
}

?>