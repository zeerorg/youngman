<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Youngman | Challan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
    
<?php
 include("includes/dbcon.php");
include("secure/db_connect.php");
$job_order = $_GET['job'];
$challan_id = $_GET['id'];
$challan_type = '';

$pickup_location_id = '';
$delivery_location_id = '';

$pickup_address = '';
$delivery_address = '';
$challan_amt = '';
$timestamp = '';
    
$query_loc_id = "SELECT * from table_challan WHERE challan_id = '$challan_id'";

$loc_ids = mysqli_query($con, $query_loc_id);
                    while($locations=mysqli_fetch_array($loc_ids)){
                        
                        $pickup_location_id = $locations['pickup_loc_id'];
                        $delivery_location_id = $locations['delivery_loc_id']  ;   //JOB Order
                            $challan_type =  $locations['type'];
                        $challan_amt =  $locations['challan_tot'];
                         $timestamp = $locations['timestamp'];
                    }


    $query_pickup_loc = "SELECT * from table_location WHERE location_id='$pickup_location_id''";

$pickup_location = mysqli_query($con, $query_pickup_loc);
    while($pick=mysqli_fetch_array($pickup_location)){
                        $pickup_address = $pick['address'].'<br>'.$pick['state']."<br>".$pick['pincode'];
                    }



if($challan_type == 1) {
    $q = "SELECT  table_quotation.delivery_address, table_quotation.delivery_pincode, qb_cache_customer.billing_address, qb_cache_customer.billing_pincode FROM table_quotation , qb_cache_customer WHERE table_quotation.job_order = ? AND table_quotation.customer_id = qb_cache_customer.customer_id";
    if ( $stmt = $mysqli->prepare( $q ) ) {
        $stmt->bind_param('s', $delivery_location_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($delivery_address, $delivery_pincode, $billing_address, $billing_pincode);
        $stmt->fetch();
    }else echo $mysqli->error;
    
    $delivery_address = $delivery_address."<br>".$delivery_pincode;
    
    
     $q = "SELECT  address, state, pincode FROM table_location WHERE location_id = ? ";
    if ( $stmt = $mysqli->prepare( $q ) ) {
        $stmt->bind_param('s', $pickup_location_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($address, $state, $pincode);
        $stmt->fetch();
    }else echo $mysqli->error;
    
    $pickup_address = $address."<br>". $state."<br>". $pincode;
    
    
}
$sql = "SELECT * FROM `challan_item_relation` WHERE challan_id = '$challan_id'";
$items = mysqli_query($con, $sql);
$si=0;
$string ='';
foreach($items as $row){
   $si++;
      $string.="<tr>
            <td>$si</td>
            <td>$row[item_id]</td>
            <td>$row[item_description]</td>
            <td>$row[quantity]</td>
            <td>$row[unit_price]</td>
            <td>$row[total_price]</td>
            </tr>";
        if($row['item_type']=="Item"){
          
        }else{
           $bundle_items =  mysqli_query($con, "SELECT * from bundle_item_relation WHERE bundle_id = '".$row[item_id]."'");
            
            foreach($bundle_items as $bundle){
                $string.="<tr>
                <td></td>
                <td>$bundle[item_id]</td>
                <td></td>
                <td>$bundle[quantity]</td>
                <td></td>
                <td></td>
                </tr>";
            }
        }
}
?>
    
<body  onload="window.print();">
<div class="wrapper">
      <!-- Main content -->
    <!-- Main content -->
     <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Youngman India Pvt. Ltd.
             <medium class="pull-right">
              <?php
          
          if($challan_type=='0'){
              echo "Sales Challan";
          }elseif($challan_type=='1'){
              echo "Delivery Challan";
          }elseif($challan_type=='2'){
              echo "Pickup Challan";
          }
          
          ?>
               <small><strong><?php echo "<br>Dated: ".date('M j Y', strtotime($timestamp)); ?></strong></small>
              </medium>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
        <hr>
        
        <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          
         <strong> Pickup Location</strong>
          <address>
             <?php echo $pickup_address;?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        <strong>  Delivery Address</strong>
          <address>
            <?php echo $delivery_address;?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        <strong>  Billing Address</strong>
          <address>
            <?php echo $billing_address."<br>".$billing_pincode;?>
          </address>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
                <th>S. No.</th>
                <th>Item Id</th>
                <th>Description</th>
             
              <th>Qty</th>
                 <th>Unit Price</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
                <?php echo $string; ?>
              <tr>
                <th>Total</th>
                <th></th>
                <th></th>
              <th></th>
              <th></th>
                <th>₹  <?php echo  $challan_amt;?> </th>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
           <div class="row">
          

<table style="width:100%" class="table">
  <tr>
    <td colspan="2"><b>Company's VAT TIN:</b> 09866108354</td>
    <td>For Youngman India Pvt. Ltd.</td>
  </tr>
  <tr>
    <td colspan="2"><b>Company's Service TAX No:</b> AAACY4840MSD001</td>
    <td></td>
  </tr>
   <tr>
    <td colspan="2"><b>Company's PAN:</b> AAACY4840M</td>
    <td>Authorized signatory</td>
  </tr>
</table>
    </div>
         

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="print_challan.php?id=<?php echo $_POST['challan_id']; ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                   <?php if ($_SESSION['role']=="planning"): ?>
            <form method="post" action="quash_challan.php">
            <input type="hidden" name="challan_id" value="<?php echo $challan_id; ?>">           
            <button type="submit" class="btn btn-danger pull-right" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Quash Challan
            </button>
            </form>
             <?php endif; ?>
        </div>
      </div>
    </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
