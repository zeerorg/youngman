<?php include ("includes/header.php");?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Challans </h3>
                    </div>
                    <!-- /.box-header -->



                    <div class="modal" id="my_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">Add Recieving</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="add_recieving.php" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="challan">Challan ID</label>
                                            <input class="form-control" type="text" id="challan" name="bookId" value="" required="true" readonly="true">

                                        </div>
                                        <div class="form-group">
                                            <label for="job_order">Job Order</label>
                                            <input class="form-control" type="text" id="job_order" name="jobOrder" value="" required="true" readonly="true">

                                        </div>

                                        <input type="file" name="fileToUpload" id="fileToUpload" required="true">

                                        <div class="form-group">
                                            <label for="transporter">Transporter</label>
                                            <input class="form-control" type="text" name="transporter" id="transporter" placeholder="Transporter Name" required="true">

                                        </div>
                                        <div class="form-group">
                                            <label for="name">Recieving Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>

                                                <input class="form-control" name="recieving_date" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" type="text" class="form-control pull-right">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="gr_no">GR No</label>
                                            <input class="form-control" type="text" name="gr_no" id="gr_no" placeholder="GR Number" required="true">

                                        </div>
                                        <div class="form-group">
                                            <label for="amt">Transport Amount</label>
                                            <input class="form-control" type="text" name="amt" id="amt" placeholder="Transport Amount" required="true">

                                        </div>
                                        <input type="submit" name="submit" value="Add Recieving">
                                    </form>
                                </div>



                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="box-body">
                        <table id="godown" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Type of challan</th>
                                    <th>Challan No</th>
                                    <th>Delivery/Pickup Date</th>
                                    <th>Job Order</th>
                                    <th>View Challan</th>
                                    <th>Upload Recieving</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                              //  echo $_SESSION['username'];
             
                      $sql = "SELECT table_challan.type, table_challan.challan_id, table_challan.job_order,table_quotation.delivery_date FROM table_challan,table_quotation WHERE table_challan.job_order=table_quotation.job_order AND table_challan.status=0 AND ( table_challan.pickup_loc_id = ? OR table_challan.delivery_loc_id = ?)";
                        if($stmt = $mysqli->prepare($sql)){
                            $stmt->bind_param('ss',   $_SESSION['username'],  $_SESSION['username']);
                            $stmt->execute();
                            $stmt->store_result();
                            $stmt->bind_result($type, $challan_id, $job_order, $delivery_date);
                        }else echo $mysqli->error;
                       
		           		while ($stmt->fetch()) {
                ?>
                                    <tr>

                                        <td>
                                            <?php if($type=="1") echo "<span class='label label-success'>Delivery</span>";
                            else echo "<h4><span class='label label-danger'>Pickup</span></h4>"; ?></td>
                                        <td>
                                            <?php echo $challan_id; ?>
                                        </td>
                                        <td>
                                            <?php echo $delivery_date; ?>
                                        </td>
                                        <td>
                                            <?php echo $job_order; ?>
                                        </td>
                                        <td>
                                             <?php if($type=="1") { ?>
                                            <form action="view_challan.php?id=<?php echo $job_order; ?>" method="post">

                                                <input type="hidden" name="challan_id" id="challan_id" value="<?php echo $challan_id; ?>">

                                                <button class="btn btn-info" type="submit" name="formpdf_btn"><i class="fa fa-print"></i> View</button>
                                            

                                                
                                            </form>
                         <?php } ?>

                                        </td>
                                        <td>
                                             <?php if($type=="1") { ?>
                                            <a href="#my_modal" data-toggle="modal" data-book-id="<?php echo $challan_id; ?>" data-job-order="<?php echo $job_order;?>">Add Recieveing</a>
                                            <?php }else { ?>
                                            
                                             <form action="recieving_form.php" method="post">
                                                <input type="hidden" name="job_order" id="job_order" value="<?php echo $job_order; ?>">
                                                <input type="hidden" name="challan_id" id="challan_id" value="<?php echo $challan_id; ?>">

                                                <button class="btn btn-info" type="submit" name="recieve_btn">Complete Pickup </button>
                                               

                                            </form>
            
                                            
                                            <?php } ?>

                                        </td>
                </tr>
                <?php 
                }
                ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Type of challan</th>
                        <th>Challan No</th>
                        <th>Delivery/Pickup Date</th>
                        <th>Job Order</th>
                        <th>View Challan</th>
                        <th>Upload Recieving</th>
                    </tr>
                </tfoot>
                </table>
            </div>


        </div>
        <!-- /.box -->
</div>
<!-- /.col -->

<!-- /.col -->
</div>
<!-- /.row -->
</section>


<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>

<!-- If recieving successfull show everything ok info -->
                      <?php if(isset($_GET['success'])) {?>
                        <script>
                        showAlert("Success", "Recieving Added", "success");
                        </script>
                        <?php }?>

                    <!-- if registration error show this -->
                        <?php if(isset($_GET['error'])) {?>
                        <script>
                        showAlert("Error", "Oops! Some error ocurred.", "error");
                        </script>
                        <?php }?>  