

  <?php  include 'includes/header.php';
            if(login_check($mysqli) == true) { ?>

     <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
        <div class=" nav-tabs-custom">
   <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#menu1">Orders</a></li>
    <li><a data-toggle="tab" href="#menu2">Quotations</a></li>
  </ul>
        
<div class="tab-content">
    <div id="menu1" class="tab-pane fade in active">
      <h3>Orders</h3>
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Orders</h3>
            </div>
            <!-- /.box-header -->
      
           
                
               <div class="box-body">
              <table id="orders" class="table table-bordered table-hover">
                <thead>
                <tr>
                
                  <th>Customer Name</th>
                  <th>Job Order</th>
                  <th>View Order</th>
                </tr>
                </thead>
                <tbody>
                <?php
                 if($stmt = $mysqli->prepare("SELECT customer_name, job_order, s_no  FROM table_quotation WHERE status = 'order' AND   createdby = ? ORDER BY timestamp DESC")){
                   $stmt->bind_param('s',  $_SESSION['user_email']); // Bind "$email" to parameter.
                   $stmt->execute(); // Execute the prepared query.
                   $stmt->store_result();
                   $stmt->bind_result($customer_name, $job_order, $s_no); // get variables from result.
                  // $stmt->fetch();                            
                                              
                while($stmt->fetch())
                {
                ?>
                <tr> 
                    
                  <td><?php echo $customer_name; ?></td>
                     <td><?php echo $job_order; ?></td>
                <td><a class="btn btn-block btn-default" href="vieworder.php?id=<?php echo $s_no; ?>"><i class="fa fa-eye"></i> View</a></td>
                </tr>
                <?php 
                }
                       $stmt->close();                      }else {echo "ERROR";}
                ?>
                </tbody>
                <tfoot>
                <tr>
                
                  <th>Description</th>
                  <th>Job Order</th>
                  <th>View Order</th>
                </tr>
                </tfoot>
              </table>
            </div>
      
           
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
   
        <!-- /.col -->
      </div>
    </div>
    <div id="menu2" class="tab-pane fade">
            <h3>Quotations</h3>
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Quotations</h3>
            </div>
            <!-- /.box-header -->
      
             <div class="box-body">
              <table id="godown" class="table table-bordered table-hover">
                <thead>
               <tr>
                   <th>ID</th>
                  <th>Type</th>
                  <th>Customer Name</th>
                  <th>Edit</th>
                    <th>Create Order</th>
                   <th>View</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>     
                      <?php
                 if($stmt = $mysqli->prepare("SELECT q.s_no, q.type, q.customer_name,q.total, c.credit_limit, c.outstanding FROM table_quotation AS q, qb_cache_customer AS c WHERE status='quot' AND createdby = ? AND c.customer_id = q.customer_id ORDER BY timestamp DESC")){
                   $stmt->bind_param('s',  $_SESSION['user_email']); // Bind "$email" to parameter.
                   $stmt->execute(); // Execute the prepared query.
                   $stmt->store_result();
                   $stmt->bind_result($s_no, $type, $customer_name, $total, $credit_limit, $outstanding); // get variables from result.
                  // $stmt->fetch();                            
                                              
                while($stmt->fetch())
                {
                ?>
                    
                <tr>
                    <td class="sNo"><?php echo $s_no; ?></td>
                       <td><?php echo $type; ?></td>
                     <td><?php echo $customer_name; ?></td>
                    <td> <a class="btn btn-block btn-default" <?php 
                           if($type=='Rental'){ echo "href=edit_rental_quotation.php?id=".$s_no; }
                            elseif($type=='Sales'){ echo "href=edit_sales_quotation.php?id=".$s_no;}
                           
                           ?>><i class="fa fa-edit"></i> Edit</a></td>
                <td><a class="btn btn-block btn-default" href="createorder.php?id=<?php echo $s_no; ?>"><i class="fa fa-eye"></i> Create</a></td>
                    <td><a class="btn btn-block btn-default" href="<?php 
                        if($type=="Rental"){echo "viewquotation.php?id=".$s_no;}else{echo "view_sales_quotation.php?id=".$s_no;}
                        ?>"><i class="fa fa-eye"></i> View</a></td>
                    <td><button type="button" class="btn btn-block btn-danger delete_btn">Delete</button></td>
                </tr>
                <?php 
                }  $stmt->close();                      }else {echo "ERROR";}
                ?>
               
                    
                </tbody>
                <tfoot>
                <tr>
                     <th>ID</th>
                  <th>Status</th>
                  <th>Customer Name</th>
                  <th>Edit</th>
                    <th>Create Order</th>
                     <th>View</th>
                    <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
                
           
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
   
        <!-- /.col -->
      </div>
    </div>
            
            
        </div>
            
        </div>
      <!-- /.row -->
      <!-- Main row -->
     
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    <?php
             include 'includes/footer.php';
            
            } else {
   echo 'You are not authorized to access this page, please login. <br/>';
    header("Location: 'secure_login.php");
}
   
    ?>
<script>
    
    $(".delete_btn").on('click', function() {
	var s_no = $(this).closest('tr').children('td.sNo').text();
        
       // alert(s_no);
        
        swal({
          title: "Are you sure?", 
          text: "Are you sure that you want to delete this quotation?", 
          type: "warning",
          showCancelButton: true,
          closeOnConfirm: false,
          confirmButtonText: "Yes, delete quotation!",
          confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax(
                    {
                        type: "POST",
                        url: "ajax/delete_quotation.php",
                        data: 'quot_no='+s_no,
                        success: function(data){
                        }
                    }
            )
          .done(function(data) {
            swal("Success!", "Quotation has been deleted!", "success");
                location.reload();

          })
          .error(function(data) {
            swal("Oops", "We couldn't connect to the server!", "error");
          });
        });

        
        
});
</script>