<?php

require_once dirname(__FILE__) . '/config.php';

require_once dirname(__FILE__) . '/views/header.tpl.php';

?>

<pre>

<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);


include 'secure/db_connect.php';
include 'secure/functions.php';
include 'qb_functions.php';

if ( isset($_POST) ) { 
                 
            $firstName          =  $_POST['firstName'];
            $lastName           = $_POST['lastName'];

            $billAddLine        = $_POST['billAddLine'];
            $billAddLine2       = $_POST['billAddLine2'];
            $billAddCity        = $_POST['billAddCity'];
            $billAddPin         = $_POST['billAddPin'];

            $mailAddLine        = $_POST['mailAddLine'];
            $mailAddLine2       = $_POST['mailAddLine2'];
            $mailAddCity        = $_POST['mailAddCity'];
            $mailAddPin         = $_POST['mailAddPin'];
         
            $company            = $_POST['company'];
            $mailing_address    = $mailAddLine.' '.$mailAddLine2.' '.$mailAddCity;
         
            $email              = $_POST['email'];
            $phone              = $_POST['primPhone'];
            $altPhone           = $_POST['altPhone'];
            $mobile             = $_POST['mobile'];
            $gst                = 'GSTN '.$_POST['gst'];
            $cin                = 'CIN '.$_POST['cin'];
         
            $creditTerm         = $_POST['creditTerm'];
            $creditLimit        = $_POST['creditLimit'];
         
            $securityLetter     = $_POST['securityLetter'];
         
            $rentalAdvance      = $_POST['rentalAdvance'];
            $securityLetter     = $_POST['securityLetter'];
            $rentalOrder        = $_POST['rentalOrder'];
            
            $securityCheque     = $_POST['securityCheque'];
            $category           = $_POST['category'];
            
         
            $CustomerService = new QuickBooks_IPP_Service_Customer();

            $Customer = new QuickBooks_IPP_Object_Customer();
            $Customer->setGivenName($firstName);
            $Customer->setLastName($lastName);
         
            $Customer->setDisplayName($company);
            $Customer->setCompanyName( $company);

            // Phone #
            $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
            $PrimaryPhone->setFreeFormNumber($phone);
            $Customer->setPrimaryPhone($PrimaryPhone);

            // Mobile #
            $Mobile = new QuickBooks_IPP_Object_Mobile();
            $Mobile->setFreeFormNumber($mobile);
            $Customer->setMobile($Mobile);


            // Bill address
            $BillAddr = new QuickBooks_IPP_Object_BillAddr();
            $BillAddr->setLine1($billAddLine);
            $BillAddr->setLine2($billAddLine2);
            $BillAddr->setLine3($gst);
            $BillAddr->setLine4($cin);
            $BillAddr->setCity($billAddCity);
            $BillAddr->setCountrySubDivisionCode('DL');
            $BillAddr->setPostalCode($billAddPin);
            $Customer->setBillAddr($BillAddr);
         
            // Ship address supposed to be mailing address for sending bills
            $ShipAddr = new QuickBooks_IPP_Object_ShipAddr();
            $ShipAddr->setLine1($mailAddLine);
            $ShipAddr->setLine2($mailAddLine2);
            $ShipAddr->setCity($mailAddCity);
            $ShipAddr->setCountrySubDivisionCode('DL');
            $ShipAddr->setPostalCode($mailAddPin);
            $Customer->setShipAddr($ShipAddr);

            // Email
            $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
            $PrimaryEmailAddr->setAddress($email);
            $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);  
         

            if ($resp = $CustomerService->add($Context, $realm, $Customer))
            {
                 $id = QuickBooks_IPP_IDS::usableIDType($resp);
                ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
                
                
                print('Our new customer ID is: [' . $id . '] (name "' . $Customer->getDisplayName() . '")');
                //populate local db for cust local
               if ($insert_stmt = $mysqli->prepare("INSERT INTO `customer_local`    (customer_id ,category, mailing_address , mailing_pincode, credit_limit, security_letter, rental_advance, rental_order, security_check, credit_term, gst_no, cin_no) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"))
{
$insert_stmt->bind_param('ssssssssssss',  $id, $category, $mailing_address,  $mailAddPin, $creditLimit, $securityLetter, $rentalAdvance, $rentalOrder, $securityCheque, $creditTerm, $gst, $cin);
$insert_stmt->execute();
                   printf("<br>Error: %s\n", $insert_stmt->error);
}
else echo $mysqli->error;
                
                echo "<br>INSERT INTO `customer_local`    (customer_id ,category, mailing_address , mailing_pincode, credit_limit, security_letter, rental_advance, rental_order, security_check, credit_term, gst_no, cin_no) VALUES ($id, $category, $mailing_address,  $mailAddPin, $creditLimit, $securityLetter, $rentalAdvance, $rentalOrder, $securityCheque, $creditTerm, $gst, $cin)";
                
                refreshCustomerCache();
                
                header("Location: create_customer.php?success=".$id);
            }
            else
            {
                print($CustomerService->lastError($Context));
                //Make toast tht it failed
                 header("Location: create_customer.php?error=".$CustomerService->lastError($Context));
            }

        
         }





?>

</pre>

<?php

require_once dirname(__FILE__) . '/views/footer.tpl.php';
