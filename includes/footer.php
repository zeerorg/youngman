<!--FOOTER-->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2017 All rights
        reserved.</strong>
  </footer>
</div>
<!-- ./wrapper -->
    
<!--Scripts-->
    <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script> 
 <!-- jQuery 2.2.0 -->

<script src="plugins/sweetalert/sweetalert.min.js"></script>
<link rel="stylesheet" href="plugins/sweetalert/sweetalert.css">
<!--Sweet Alert -->

<script src="plugins/confirm/jquery-confirm.min.js"></script>
<link rel="stylesheet" href="plugins/confirm/jquery-confirm.min.css">


<script src="plugins/select2/select2.full.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="dist/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

  <script src="dist/js/jquery-ui.min.js"></script>
<script>

function get_movement(){
    var data = $('.check').val();
    alert(data);
    
      $.ajax({
            "type":"POST",
            "url":"movement_analysis.php",
            data:'job_order='+data,
            "success":function(data){
               /* $("#location_details")
                    .html(data);*/
            }
        });
    
}

</script>

<script>
  $(function () {
    $('.select2').select2();
    $('#shop').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
    $('#godown').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
    $('#orders').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
    $('#quotations').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
    $('#pending').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    $('#finished').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
      
    
</script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>


<script>
$('#datepicker').datepicker({
	showButtonPanel: true,
	todayHighlight: true,
	format: 'dd-mm-yyyy',
  autoclose: true
});
    
   
var pic_date = $('#pic_date').datepicker({
  showButtonPanel: true,
  todayHighlight: true,
  format: 'dd-mm-yyyy',
  autoclose: true
})
    // }).on("changeDate", function(){
    //     console.log("pick date selected")
    //     console.log($("#pic_date").val())
    //     console.log($("#del_date").val())
    //     if( $("#pic_date").datepicker('getDate') < $('#del_date').datepicker("getDate") ) {
    //         console.log("wrong date")
    //         $("#pic_date").parent().parent().addClass("has-error")
    //     }
    //   });
var del_date = $('#del_date').datepicker({
  showButtonPanel: true,
  todayHighlight: true,
  format: 'dd-mm-yyyy',
  autoclose: true,
});

del_date.on("changeDate", function(){
  $(".del_date_buttons").removeClass("hidden")
});

function addToPickDate(months) {
  var toSet = del_date.datepicker('getDate')
  toSet.setMonth(toSet.getMonth() + months)
  pic_date.datepicker('setDate', toSet)
}
</script>
<script>

</script>


<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>



<script>

$('#my_modal').on('show.bs.modal', function(e) {
    var bookId = $(e.relatedTarget).data('book-id');
    $(e.currentTarget).find('input[name="bookId"]').val(bookId);
    
    var challanType =  $(e.relatedTarget).data('challan-type');
     $(e.currentTarget).find('input[name="challanType"]').val(challanType);
    
    var jobOrder =  $(e.relatedTarget).data('job-order');
     $(e.currentTarget).find('input[name="jobOrder"]').val(jobOrder);
});
    
    $('#extend_modal').on('show.bs.modal', function(e) {
    
    var jobOrder =  $(e.relatedTarget).data('job-order');
     $(e.currentTarget).find('input[name="jobOrder"]').val(jobOrder);
});
    
     $('#pickup_modal').on('show.bs.modal', function(e) {
    
    var jobOrder =  $(e.relatedTarget).data('job-order');
     $(e.currentTarget).find('input[name="jobOrder"]').val(jobOrder);
});

function showAlert(title, text, type){
  swal(title, text, type)
}


</script>
</body>
</html>