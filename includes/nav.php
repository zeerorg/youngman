 <!--NAV-->
      <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['user_email']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
       
          
           <?php if ($_SESSION['role']=="sales"): ?>
           <li class="treeview <?php if($currentPage =='dashboard_sales' ){echo 'active';}?>">
          <a href="dashboard_sales.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
         
             <li class="treeview <?php if($currentPage =='addRentalQuotation' ){echo 'active';}?>">
          <a href="addRentalQuotation.php">
            <i  class="fa fa-edit"></i> <span>Rental Quotation</span>
          </a>
        </li>
         
          
          <?php endif; ?>
          
            <?php if ($_SESSION['role']=="sales_head"): ?>
           <li class="treeview <?php if($currentPage =='dashboard_sales' ){echo 'active';}?>">
          <a href="dashboard_sales.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
         
             <li class="treeview <?php if($currentPage =='addRentalQuotation' ){echo 'active';}?>">
          <a href="addRentalQuotation.php">
            <i  class="fa fa-edit"></i> <span>Rental Quotation</span>
          </a>
        </li>
            <li class="treeview <?php if($currentPage =='dashboard_sales_head' ){echo 'active';}?>">
          <a href="dashboard_sales_head.php">
            <i  class="fa fa-edit"></i> <span>View All</span>
          </a>
        </li>
         
          
          <?php endif; ?>
          
          
          
            <?php if ($_SESSION['role']=="sales_coor"): ?>
         <!--  <li class="treeview <?php if($currentPage =='dashboard' ){echo 'active';}?>">
          <a href="dashboard_sales.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>-->
          <li class="treeview <?php if($currentPage =='dashboard_sales' ){echo 'active';}?>">
          <a href="dashboard_sales.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview <?php if($currentPage =='addRentalQuotation' ){echo 'active';}?>">
          <a href="addRentalQuotation.php">
            <i  class="fa fa-edit"></i> <span>Rental Quotation</span>
          </a>
        </li>
          
           <li class="treeview <?php if($currentPage =='create_customer' ){echo 'active';}?>">
          <a href="create_customer.php">
            <i class="fa fa-dashboard"></i> <span>Create Customer</span>
          </a>
        </li>
        <!--   <li class="treeview <?php if($currentPage =='invoice_challan' ){echo 'active';}?>">
          <a href="invoice_challan.php">
            <i class="fa fa-dashboard"></i> <span>Invoice</span>
          </a>
        </li> -->
             <li class="treeview <?php if($currentPage =='ini_pickup' ){echo 'active';}?>">
          <a href="ini_pickup.php">
            <i class="fa fa-dashboard"></i> <span>Pickup</span>
          </a>
        </li>
    

        

         
          
          <?php endif; ?>
          
           <?php if ($_SESSION['role']=="planning"): ?>
          
          <li class="treeview <?php if($currentPage =='dashboard_planning' ){echo 'active';}?>">
          <a href="dashboard_planning.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
            <li class="treeview <?php if($currentPage =='billable_material' ){echo 'active';}?>">
          <a href="billable_material.php">
            <i class="fa fa-dashboard"></i> <span>Billable</span>
          </a>
        </li>
            <li class="treeview <?php if($currentPage =='warehouse_material' ){echo 'active';}?>">
          <a href="warehouse_material.php">
            <i class="fa fa-dashboard"></i> <span>Warehouse Material</span>
          </a>
        </li>
            <li class="treeview <?php if($currentPage =='job_material' ){echo 'active';}?>">
          <a href="job_material.php">
            <i class="fa fa-dashboard"></i> <span>Job Material</span>
          </a>
        </li>
          
          
           
         
          
          <?php endif; ?>
          
          
     <?php if ($_SESSION['role']=="movement"): ?>
          
          <li class="treeview <?php if($currentPage =='dashboard_movement' ){echo 'active';}?>">
          <a href="dashboard_movement.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
          
          
           
         
          
          <?php endif; ?>
             <li class="treeview <?php if($currentPage =='logout' ){echo 'active';}?>">
          <a href="logout.php">
            <i  class="fa fa-back"></i> <span>Logout</span>
          </a>
        </li>
          
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<!--/NAV-->