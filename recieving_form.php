
<?php include ("includes/header.php");
$job_order = $_POST['job_order'];
$challan_id = $_POST['challan_id'];
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Recieving Form
        <small>Provide complete details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Add Recieving</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
            <form action="add_recieving.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="jobOrder" id="JobOrder" value="<?php echo $job_order; ?>" >
                <input type="hidden" name="bookId" value="<?php echo $challan_id; ?>" >
                <input type="hidden" name="warehouse_id" value="<?php echo $_SESSION['username']; ?>" >
        <div class="box-header with-border">
          <h3 class="box-title">Recieving Form</h3>
        </div>
        <div class="box-body">
           <div class='row'>
                                    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                                        <table class="table table-bordered table-hover" id="table_recieving">
                                            <thead>
                                                <tr>
                                                    <th width="2%"><input id="check_all" type="checkbox" /></th>
                                                    <th width="30%">Item ID</th>
                                                    <th width="34%">Description</th>
                                                    <th width="10%">OK Qty</th>
                                                    <th width="10%">Missing</th>
                                                    <th width="10%">Damage</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
            
            <button class="btn btn-danger deleterow" type="button">- Delete</button>
                                        <button class="btn btn-success addnewrow" type="button">+ Add More</button>
        </div>
        <!-- /.box-body -->
          
        <div class="box-footer">
           
             <input type="file" name="fileToUpload" id="fileToUpload" required="true">

                                        <div class="form-group">
                                            <label for="transporter">Transporter</label>
                                            <input class="form-control" type="text" name="transporter" id="transporter" placeholder="Transporter Name" required="true">

                                        </div>
                                        <div class="form-group">
                                            <label for="name">Recieving Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>

                                                <input class="form-control" name="recieving_date" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" type="text" class="form-control pull-right">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="gr_no">GR No</label>
                                            <input class="form-control" type="text" name="gr_no" id="gr_no" placeholder="GR Number" required="true">

                                        </div>
                                        <div class="form-group">
                                            <label for="amt">Transport Amount</label>
                                            <input class="form-control" type="text" name="amt" id="amt" placeholder="Transport Amount" required="true">

                                        </div>
           <button type="submit" name="recieving_submit" id="submit" class="btn btn-primary">Submit</button>
            
        </div>
                                   
                                        
                                    
          
        <!-- /.box-footer-->
                  <form action="add_recieving.php" method="post" enctype="multipart/form-data">
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
<script src="dist/js/recieving_form.js"></script>