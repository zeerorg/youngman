<?php include ("includes/header.php");?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      
            <link href="dist/css/jquery-ui.min.css" rel="stylesheet">
    
      
      
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Billable Material at Job Order
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Billable</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        <div class="input-group input-group-sm col-sm-4">
                <div class="form-group">
                                            <label>Select Job Order</label>
                                            <select class="form-control check2">
                                                
                                                <option>Select</option>
                                                
                                                <?php 
                                                $job_order = "SELECT location_id FROM table_location WHERE location_type='joborder'";
                                                
                                                if($Items  = $mysqli->prepare( $job_order )){
                                                    $Items->bind_param('s',$challan_id);
                                                    $Items ->execute();
                                                    $Items ->store_result();
                                                    $Items ->bind_result($job_order);   

                                                    }else echo $mysqli->error;

                                                while( $Items->fetch()){
                                                    
                                                    echo '<option value="'.$job_order.'">'.$job_order.'</option>';
                                                }
                                                
                                                ?>
                                                
                                                
                                            </select>
                                        </div>
              </div>

        <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Items</h3>
            </div>
            <!-- /.box-header -->
              <div id="location_details" class="box-body table-responsive no-padding">
            
         
                  
              </div>
              
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
        
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>
<script src="dist/js/billable_material.js"></script>