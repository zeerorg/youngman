<?php

//include("includes/dbcon.php");
include("secure/db_connect.php");
include dirname(__FILE__) . '/qb_functions.php';

mysqli_autocommit($mysqli, false);

$challan_id = $_POST['bookId'];
$transporter = $_POST['transporter'];
$gr_no = $_POST['gr_no'];
$amt = $_POST['amt'];
$recieving = '';
$job_order = $_POST['jobOrder'];
$current_date = date("Y-m-d"); 
$current_time = date("H:i:s");

$recieving_date = $_POST['recieving_date'];
$pre =  false;

   //Check if it is pre
       
       $q = "SELECT customer_local.category FROM customer_local,table_quotation WHERE customer_local.customer_id = table_quotation.customer_id AND table_quotation.job_order = ? ";
       if($st = $mysqli->prepare($q)){
        $st->bind_param('s', $job_order);
        $st ->execute();
        $st ->store_result();
        $st ->bind_result($category);   
        $st->fetch();
        }else echo $mysqli->error;
       
       if($category=="Pre"){
    
            $pre =  true;
       }
       


if(isset($_POST["recieving_submit"]) ){
    $code = $_POST['code'];
    $desc = $_POST['desc'];
    $ok_qty = $_POST['ok_qty'];
    $missing_qty =$_POST['missing_qty'];
    $damage_qty = $_POST['damage_qty'];
    $warehouse_id = $_POST['warehouse_id'];
    
    
    for($i=0; $i<count($code); $i++){
        
      $fake_ch = "INSERT INTO  challan_item_relation (challan_id, item_id, quantity, missing, damage, job_order) VALUES(?, ?, ?, ?, ?, ? ) ON DUPLICATE KEY UPDATE quantity = quantity + VALUES(quantity), missing = missing + VALUES(missing) , damage = damage + VALUES(damage)";

          if($fc = $mysqli->prepare($fake_ch)){
            $fc->bind_param('ssssss' , $challan_id, $code[$i], $ok_qty[$i], $missing_qty[$i], $damage_qty[$i], $job_order );
            $fc->execute();
          }else echo $mysqli->error;

   $fake_q = "UPDATE location_item_relation SET quantity = quantity - ( ? + ? ) WHERE location_id = ? AND item_id = ? ";
       
        if($fc = $mysqli->prepare($fake_q)){
            $fc->bind_param('ssss',$ok_qty[$i], $damage_qty[$i], $job_order, $code[$i] );
            $fc->execute();
          }else echo $mysqli->error;
     
       $fake_s = "UPDATE location_item_relation SET quantity = quantity + ?, damaged =damaged + ? WHERE location_id = ? AND item_id = ? ";

        if($fc = $mysqli->prepare($fake_s)){
            $fc->bind_param('ssss',$ok_qty[$i], $damage_qty[$i], $warehouse_id, $code[$i] );
            $fc->execute();
          }else echo $mysqli->error;        
        
    }
    
    if(!addEstimate($challan_id, $job_order)){
            //HANDLE FAILURE OF ESTIMATE CREATION
        }
        //stop billing
    $stop_billing = "UPDATE table_billing set active = 0 where ";
}


$recieving_date = strtotime($recieving_date);

$recieving_date = date("Y-m-d", $recieving_date); //SQL format date


$target_dir = "uploads/";
  $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $extension = end(explode(".", $_FILES["fileToUpload"]["name"]));
    $path=md5($current_date.$current_time).'.'.$extension;

$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])||isset($_POST["recieving_submit"])  ) {
 $uploadOk = 1;
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "pdf"   ) {
    echo "Sorry, only JPG, JPEG, PNG, GIF & PDF files are allowed. Your file is a ".$imageFileType;
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} 
else {
    
    
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "uploads/".$path))
    {
        
        $recieving = "uploads/". $path;
              
        
        $query = "UPDATE table_challan 
                    SET recieving = ?,
                    recieving_date = ?,
                    transporter = ?, 
                    gr_no = ?, 
                    amt = ?,
                    status = 1
                    WHERE challan_id = ? ";
        
        if($jo  = $mysqli->prepare( $query )){
        $jo ->bind_param('ssssss',$recieving, $recieving_date,$transporter, $gr_no, $amt, $challan_id);
        
        }else echo $mysqli->error;
        
        
if($jo ->execute())
{
    
    $select_items = "SELECT item_code FROM invoice_item_feed WHERE job_order = ? AND challan_id= ? ";
    
      if($stmt  = $mysqli->prepare( $select_items )){
        $stmt ->bind_param('ss',$job_order, $challan_id);
        $stmt ->execute();
        $stmt ->store_result();
        $stmt ->bind_result($item_code);   
        }else echo $mysqli->error;
    
        $last_start_date = "0000-00-00";
        $last_end_date = $recieving_date;
        $month_end = new DateTime('last day of this month');
        $month_end = $month_end->format('Y-m-d');
        $first_day_of_next_month = date("Y-m-d", strtotime("+1 day", strtotime($month_end)));
        
        if($pre){
            $last_start_date = $recieving_date;
            $last_end_date = $first_day_of_next_month;
        }
    
    
    
    while($stmt ->fetch()){
        $ic = $item_code;
  
         $select_from_invoice = "SELECT invoice_item_feed.qty, table_quotation_item.unit_price, table_quotation.pickup_date, table_quotation.duration_billing, customer_local.category FROM invoice_item_feed, table_quotation, table_quotation_item, customer_local WHERE invoice_item_feed.job_order = ? AND invoice_item_feed.challan_id = ? AND invoice_item_feed.item_code = ? AND invoice_item_feed.job_order = table_quotation.job_order AND table_quotation.s_no = table_quotation_item.s_no AND table_quotation_item.item_code = ? AND table_quotation.customer_id = customer_local.customer_id";

         if($s = $mysqli->prepare($select_from_invoice)){
            $s ->bind_param('ssss',$job_order, $challan_id, $item_code, $ic);
            $s ->execute();
            $s ->store_result();
            $s ->bind_result($qty, $unit_price, $pickup_date, $duration_billing, $category );   


         }else echo $mysqli->error;
        while($s->fetch()){


            $query1 = "INSERT INTO table_billing (job_order, challan_no, category, pickup_date, period, item_code, unit_price, start_date, qty, last_end_date, last_start_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";
            
     if($sm = $mysqli->prepare($query1)){
            $sm ->bind_param('sssssssssss',$job_order, $challan_id, $category, $pickup_date, $duration_billing, $ic, $unit_price, $recieving_date, $qty, $last_end_date, $last_start_date );
            $sm ->execute();
             printf("Error: %s.\n", $sm->error);
             }else echo $mysqli->error;
            
        }
    }
    
    
    
   if(create_journal_entry($amt, $challan_id, $gr_no, $transporter, $job_order)){
        mysqli_commit($mysqli);
    
       if($pre)  first_invoice_pre($job_order, $challan_id, $recieving_date);
       
      
       header("Location: dashboard_movement.php?success=1");
   }else{
       mysqli_rollback($mysqli);
       header("Location: dashboard_movement.php?error=cannot create journal entry");
   }
    
  //  
} else{
    echo "ERROR<br>";    
    echo $query;
}
        
    }
    else
    {
         echo "Sorry, there was an error uploading your file.";
         header("Location: dashboard_movement.php?error=error uploading your file.");
       
    }
}
?>