<?php

function refreshCustomerCache() 
{
require dirname(__FILE__) . '/config.php';

include("secure/db_connect.php");
//include 'secure/functions.php';

//ini_set('display_errors', 'On');
//error_reporting(E_ALL);

$CustomerService = new QuickBooks_IPP_Service_Customer();

$customers = $CustomerService->query($Context, $realm, "SELECT * FROM Customer WHERE Job = false  MAXRESULTS 1000");

foreach ($customers as $Customer)
{
    
    // print_r ($Customer);
    	$billaddr = $Customer->getBillAddr();
    
    $billing_addr = '';
    $billing_pin = '';
    if($billaddr != null){
        $billing_addr .= $billaddr->getLine1() . "\n<br>";
        $billing_addr .= $billaddr->getLine2() . "\n<br>";
        //$billing_addr .= $billaddr->getLine3() . "\n<br>";
        $billing_addr .= $billaddr->getCity() . ',  ' . $billaddr->getCountrySubDivisionCode() . ' ' . $billaddr->getPostalCode() . "\n<br>";
        $billing_addr .= $billaddr->getCountry();
          $billing_pin = strip_tags($billaddr->getPostalCode());
    }
    
    $name = strip_tags( $Customer->getFullyQualifiedName());
    
    $balance = strip_tags( $Customer->getBalanceWithJobs());
    
    $id = strip_tags($Customer->getId());
  
    $id = QuickBooks_IPP_IDS::usableIDType($id);
    
  if($billing_addr == '') $billing_addr = ' ';
  if($billing_pin == '') $billing_pin = ' ';
    
    $query = "INSERT INTO 
    qb_cache_customer (customer_id,customer_name, billing_address, billing_pincode, outstanding) 
    VALUES 
    (?, ?, ?, ?, ?)
    ON DUPLICATE KEY UPDATE
    customer_name = VALUES(customer_name),
    billing_address = VALUES(billing_address),
    billing_pincode = VALUES(billing_pincode),
    outstanding = VALUES(outstanding)
    ";
    if ($insert_stmt = $mysqli->prepare($query))
{
$insert_stmt->bind_param('sssss', $id, $name, $billing_addr, $billing_pin, $balance);
// Execute the prepared query.

        
print('Customer Id=' . $id . ' is named: ' . $name . ' located at:'.$billing_addr.' outstanding bLnce:'.$balance.'<br>');
if(!$insert_stmt->execute()) {echo $insert_stmt->error."<br><hr>";
                             echo "INSERT INTO 
    qb_cache_customer (customer_id,customer_name, billing_address, billing_pincode, outstanding) 
    VALUES 
    ($id, '$name', '$billing_addr', '$billing_pin', '$balance')
    ON DUPLICATE KEY UPDATE
    customer_name = VALUES(customer_name),
    billing_address = VALUES(billing_address),
    billing_pincode = VALUES(billing_pincode),
    outstanding = VALUES(outstanding)";}
}
else
{
// print_r ($Customer);
}

    

}

}

function addOrder()
{
    
}

function getCustomerDetails($customer_id)
{
    require dirname(__FILE__) . '/config.php';

    $CustomerService = new QuickBooks_IPP_Service_Customer();
    $customers = $CustomerService->query($Context, $realm, "SELECT * FROM Customer WHERE Id = '$customer_id'");
    
    $PrimaryPhone     = new QuickBooks_IPP_Object_PrimaryPhone();
    $Mobile           = new QuickBooks_IPP_Object_Mobile();
    $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
    $BillAddr         = new QuickBooks_IPP_Object_BillAddr();
    
    foreach ($customers as $Customer){
        $BillAddr          = $Customer->getBillAddr();   
        $PrimaryPhone      = $Customer->getPrimaryPhone();
        $Mobile            = $Customer->getMobile();
        $PrimaryEmailAddr  = $Customer->getPrimaryEmailAddr();
        }
    
     return array($BillAddr, $PrimaryPhone, $Mobile, $PrimaryEmailAddr);
}

function create_journal_entry($amount, $challan_id, $gr_no, $transporter, $job_order)
{
    
    require dirname(__FILE__) . '/config.php';
  //   require dirname(__FILE__) . '/views/header.tpl.php';
    
    $description = "Job No: ".$job_order.", Challan No : ".$challan_id." GR No : ".$gr_no." Transporter : ".$transporter;
    
    $AccountService = new QuickBooks_IPP_Service_Account(); 
    $accounts = $AccountService->query($Context, $realm, "SELECT * FROM Account WHERE Name = 'Freight Hire Business - COS'");
    
    $freight = '';
    $company = '';
    
    foreach ($accounts as $Account)
    {
	   print('Account Id=' . $Account->getId() . ' is named: ' . $Account->getFullyQualifiedName() . '<br>');
       $freight = QuickBooks_IPP_IDS::usableIDType($Account->getId());
    }
    
    //

    $account2 = $AccountService->query($Context, $realm, "SELECT * FROM Account WHERE Name = 'Transport Control Account'");
    
    foreach ($account2 as $Account)
    {
	   print('Account Id=' . $Account->getId() . ' is named: ' . $Account->getFullyQualifiedName() . '<br>');                
        $company = QuickBooks_IPP_IDS::usableIDType($Account->getId());
    }
    
    echo "Freight: ".$freight;
    echo "Company: ".$company;
    
    $JournalEntryService = new QuickBooks_IPP_Service_JournalEntry();
    
    // Main journal entry object
    $JournalEntry = new QuickBooks_IPP_Object_JournalEntry();
    //$JournalEntry->setDocNumber('1234');
    $JournalEntry->setTxnDate(date('Y-m-d'));

    // Debit line
    $Line1 = new QuickBooks_IPP_Object_Line();
    $Line1->setDescription($description);
    $Line1->setAmount($amount);
    $Line1->setDetailType('JournalEntryLineDetail');
    $Detail1 = new QuickBooks_IPP_Object_JournalEntryLineDetail();
    $Detail1->setPostingType('Debit');
    $Detail1->setAccountRef($freight);
    
    $Line1->addJournalEntryLineDetail($Detail1);
    $JournalEntry->addLine($Line1);

    // Credit line
    $Line2 = new QuickBooks_IPP_Object_Line();
    $Line2->setDescription($description);
    $Line2->setAmount($amount);
    $Line2->setDetailType('JournalEntryLineDetail');

    $Detail2 = new QuickBooks_IPP_Object_JournalEntryLineDetail();
    $Detail2->setPostingType('Credit');
    $Detail2->setAccountRef($company);

    $Line2->addJournalEntryLineDetail($Detail2);
    $JournalEntry->addLine($Line2);
    
    $JournalEntry->setCurrencyRef('INR');

    if ($resp = $JournalEntryService->add($Context, $realm, $JournalEntry))
    {
	   print('Our new journal entry ID is: [' . $resp . ']');
        return true;
    }
    else
    {
	   print($JournalEntryService->lastError($Context));
        return false;
    }

}

/*
    @param $job_order
    @param $challan_id
    @param $recieving_date IN SQL FORMAT ('Y-m-d')
*/

function first_invoice_pre($job_order, $challan_id, $recieving_date)
{
    //TODO add transaction
    //Make bill from recieving date to end of current month
    require dirname(__FILE__) . '/config.php';
  //  require dirname(__FILE__) . '/views/header.tpl.php';
    include 'secure/db_connect.php';
    
    $rental = getItemID('Rental of  Product');    
    $mobilisation = getItemID('Mobilisation and Demobilisation Costs');
    $manpower = getItemID('Manpower Costs');
    
    $month_end = new DateTime('last day of this month');
   // $last_date = $to_date->format('j-M-Y');
    $variable_for_sql_date_difference = $month_end->format('Y-m-d');
    $month_end = $month_end->format('Y-m-d');
    $first_day_of_next_month = date("Y-m-d", strtotime("+1 day", strtotime($month_end)));
    
   // echo $last_date." To date ".$to_date;
    
    
    echo "JOB ORDER: ".$job_order."<br>";
    echo "Chalan Id: ".$challan_id."<br>";
    echo "Recieve date: ".$recieving_date."<br>";
    echo "<br>Rental ".$rental."Mobilisation ".$mobilisation." Manpower ".$manpower."<br>";
    
    //get  qb_id, first_bill, freight from table_quotation
    
    $query = "SELECT qb_id, first_bill, freight FROM table_quotation WHERE job_order = ?";
    if($jo  = $mysqli->prepare( $query )){
        $jo->bind_param('s',$job_order);
        $jo ->execute();
        $jo ->store_result();
        $jo ->bind_result($qb_id, $first_bill, $freight);   
        $jo->fetch();
        }else echo $mysqli->error;
    
    echo "QB_ID      : ".$qb_id."<br>";
    echo "FIRST BILL : ".$first_bill."<br>";
    echo "FREIGHT    : ".$freight."<br>";
    
$InvoiceService = new QuickBooks_IPP_Service_Invoice();

$Invoice = new QuickBooks_IPP_Object_Invoice();
    
    $items = "SELECT item_code, qty, unit_price FROM table_billing WHERE challan_no = '$challan_id'";
    
    echo "Challan Items Query".$items."<br>";
    
    $items = "SELECT item_code, qty, unit_price FROM table_billing WHERE challan_no = ?";
    
    if($Items  = $mysqli->prepare( $items )){
        $Items->bind_param('s',$challan_id);
        $Items ->execute();
        $Items ->store_result();
        $Items ->bind_result($item_code, $qty, $unit_price);   
        //$Items->fetch();
       // echo "Row count : ".$Items->num_rows();
        }else echo $mysqli->error;
    
    while( $Items->fetch()){
        
     $ref = $rental;
        if ($item_code=="MANPOWER"){$ref = $manpower;}
        
        $description = buildDescription($item_code, $recieving_date, $month_end);
        echo $description."<br>";
        
        echo "old unit price : ".$unit_price;
        $unit_price = getUnitPrice($unit_price, $recieving_date, $month_end);
        echo "new unit price : ".$unit_price."<br>";
        
     
        $Line = new QuickBooks_IPP_Object_Line();
        $Line->setDetailType('SalesItemLineDetail');
        $Line->setAmount($qty * $unit_price);
        $Line->setDescription($description);

        $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
        $SalesItemLineDetail->setItemRef($ref);
        $SalesItemLineDetail->setUnitPrice($unit_price);
        $SalesItemLineDetail->setQty($qty);
        

        $Line->addSalesItemLineDetail($SalesItemLineDetail);

        $Invoice->addLine($Line);
        
           echo "<br>UPDATE table_billing SET last_end_date = '$first_day_of_next_month' , last_start_date = '$recieving_date' WHERE job_order = '$job_order' AND item_code = '$item_code' AND challan_no = '$challan_id'<br>";
                    
                /*    if($swap = $mysqli->prepare("UPDATE table_billing SET last_end_date = ? , last_start_date = ? WHERE job_order = ? AND item_code = ? AND challan_no = ?")){
                             $swap->bind_param('sssss', $first_day_of_next_month  ,$recieving_date  ,$job_order, $item_code, $challan_id);
                            $swap->execute();
                        }else echo $mysqli->error;*/
        
        
    }
    
      if(!$first_bill){
                echo "Freight".$freight."<br>";
                
                 $Line = new QuickBooks_IPP_Object_Line();
                    $Line->setDetailType('SalesItemLineDetail');
                    $Line->setAmount($freight * 1);
                    $Line->setDescription("One time mobilisation and demobilisation charges");
                    
                    $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
                    $SalesItemLineDetail->setItemRef($mobilisation);
                    $SalesItemLineDetail->setUnitPrice($freight);
                    $SalesItemLineDetail->setQty(1);
                    
                    $Line->addSalesItemLineDetail($SalesItemLineDetail);
                    
                    $Invoice->addLine($Line);
          
                   
          
          
            }
            

$Invoice->setCustomerRef($qb_id);
     $Invoice->setCurrencyRef('INR');
    
       $arr = explode("/",$job_order);

echo "PO:".$arr[5];
$CustomField = new QuickBooks_IPP_Object_CustomField();
$CustomField->setDefinitionId('3');
$CustomField->setName('PO');
$CustomField->setType('StringType');
$CustomField->setStringValue("PO:".$arr[5]);
$Invoice->addCustomField($CustomField);
    


if ($resp = $InvoiceService->add($Context, $realm, $Invoice))
{
	print('Our new Invoice ID is: [' . $resp . ']');
    
      echo "UPDATE table_quotation SET first_bill = 1 WHERE job_order = ".$job_order."<br>";
                        
      if($made_bill = $mysqli->prepare("UPDATE table_quotation SET first_bill = 1 WHERE job_order = ?")){
        $made_bill->bind_param('s', $job_order);
        $made_bill->execute();
        }else echo $mysqli->error;
    
}
else
{
	print($InvoiceService->lastError());
}

}
/*
    @param $type Estimate for Damaged Or Missing
*/

function addEstimate($challan_id, $job_order)
{
    require dirname(__FILE__) . '/config.php';
    include 'secure/db_connect.php';
    
    $rental = getItemID('Rental of  Product'); 
    
    echo "JOB ORDER: ".$job_order."<br>";
    echo "Chalan Id: ".$challan_id."<br>";
    echo "Item Ref : ".$rental."<br>";
    
    //get  qb_id freight from table_quotation
    
    $query = "SELECT qb_id FROM table_quotation WHERE job_order = ?";
    if($jo  = $mysqli->prepare( $query )){
        $jo->bind_param('s',$job_order);
        $jo ->execute();
        $jo ->store_result();
        $jo ->bind_result($qb_id);   
        $jo->fetch();
        }else echo $mysqli->error;
    
    echo "QB_ID      : ".$qb_id."<br>";
    
    $EstimateService = new QuickBooks_IPP_Service_Estimate();

    $Estimate = new QuickBooks_IPP_Object_Estimate();
    
    $q = "SELECT c.missing, i.item_code, i.estimate_vale FROM challan_item_relation AS c, table_item AS i WHERE c.challan_id = ? AND c.item_id = i.item_code AND c.missing !=0";
    
     if($Missing  = $mysqli->prepare( $q )){
        $Missing->bind_param('s',$challan_id);
        $Missing ->execute();
        $Missing->store_result();
        $Missing->bind_result($missing, $item_code, $estimate_vale);   
        }else echo $mysqli->error;
    
    while($Missing->fetch())
    {

    $Line = new QuickBooks_IPP_Object_Line();
    $Line->setDetailType('SalesItemLineDetail');
    $Line->setAmount($missing * $estimate_vale);
    $Line->setDescription("Missing :".$item_code. "| Qty :". $missing );

    $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
    $SalesItemLineDetail->setItemRef($rental);
    $SalesItemLineDetail->setUnitPrice($estimate_vale);
    $SalesItemLineDetail->setQty($missing);
        

    $Line->addSalesItemLineDetail($SalesItemLineDetail);

    $Estimate->addLine($Line);
    }
    
    $q = "SELECT c.damage, i.item_code, i.estimate_vale FROM challan_item_relation AS c, table_item AS i WHERE c.challan_id = ? AND c.item_id = i.item_code AND c.damage !=0";
    
     if($Damage = $mysqli->prepare( $q )){
        $Damage->bind_param('s',$challan_id);
        $Damage ->execute();
        $Damage->store_result();
        $Damage->bind_result($damage, $item_code, $estimate_vale);   
        }else echo $mysqli->error;
    
    while( $Damage->fetch())
    {

    $Line = new QuickBooks_IPP_Object_Line();
    $Line->setDetailType('SalesItemLineDetail');
    $Line->setAmount($damage * $estimate_vale);
    $Line->setDescription("Damage :".$item_code . "| Qty :". $missing );

    $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
    $SalesItemLineDetail->setItemRef($rental);
    $SalesItemLineDetail->setUnitPrice($estimate_vale);
    $SalesItemLineDetail->setQty($damage);

    $Line->addSalesItemLineDetail($SalesItemLineDetail);

    $Estimate->addLine($Line);
    }
    
    

    $Estimate->setCustomerRef($qb_id);


    if ($resp = $EstimateService->add($Context, $realm, $Estimate))
    {
        print('Our new Estimate ID is: [' . $resp . ']');
        return true;
    }
    else
    {
        print($EstimateService->lastError());
        return false;
    }
}

function getItemID($name) 
{
    require dirname(__FILE__) . '/config.php';
  //  require dirname(__FILE__) . '/views/header.tpl.php';
    $query = "SELECT * FROM Item WHERE Name = '$name'";
    $ItemService = new QuickBooks_IPP_Service_Term();
    $items = $ItemService->query($Context, $realm, $query);

    foreach ($items as $Item)
    {
        $id = QuickBooks_IPP_IDS::usableIDType($Item->getId());
        return $id;
    }
}

/*
    @param $from date Start of billing date
    @param $to_date End of billing date
*/
function buildDescription($item_name, $from_date, $to_date)
{
    $from_date = date('j-M-Y', strtotime($from_date));
    $to_date   = date('j-M-Y', strtotime($to_date));
    return  $item_name." | From:".$from_date." | To:".$to_date;
}

function getUnitPrice($unit_price, $from_date, $to_date)
{
    $date1=date_create("$from_date");
    $date2=date_create("$to_date");
    $diff=date_diff($date1,$date2);
   
    $date_difference = $diff->format("%a days");
     echo $date_difference;
     $unit_price = ($unit_price / date("t") ) * ($date_difference + 1);
    $unit_price = number_format((float)$unit_price, 2, '.', '');
    return $unit_price;
}

?>