<?php include ("includes/header.php");?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Challan</h4>
      </div>
      <div class="modal-body">
          <form method="post" action="add_pickup.php">
          <input type="hidden" name="csrf" value="<?php echo $_SESSION['login_string']; ?>" >
        <input type="text" name="job_order" id="job_order" placeholder="Job Order">
               <input type="text" name="pickup_date" id="pickup_date" placeholder="Pickup Date">
              <input type="submit" value="Submit" id="submit" name="submit">
              </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
        
        
      <!-- Info boxes -->
      <div class="col-md-4 col-sm-4 col-xs-12">
         <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">Add Pickup Challan</button>
        </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Orders</h3>
            </div>
            <!-- /.box-header -->
      <div class="modal" id="my_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Add Challan</h4>
      </div>
      <div class="modal-body">
          <form action="gui_challan.php" method="post" enctype="multipart/form-data" >
               <input type="hidden" name="csrf" value="<?php echo $_SESSION['login_string']; ?>" >
                <div class="form-group row">
                   <label  class="col-sm-2 control-label"
                              for="bookId">Job Order No</label>
                            <div class="col-sm-10">
                                <input type="text" name="bookId" value="" readonly/>
                           </div>
                </div>
              
               <div class="form-group row">
                   <label  class="col-sm-2 control-label"
                              for="challanType">Challan Type</label>
                            <div class="col-sm-10">
                                <input type="text" name="challanType" value="" readonly/>
                           </div>
                </div>
              
                 <div class="form-group row">
                   <label  class="col-sm-2 control-label"
                              for="warehouse">Warehouse</label>
                            <div class="col-sm-10">
                   <select class="custom-select form-control" name="warehouse" id="warehouse" required>
                         <option value="" selected disabled>Please select</option>
                       <?php
                       $location = "SELECT location_id, address FROM table_location WHERE location_type='warehouse'";
                        if($stmt = $mysqli->prepare($location)){
                            $stmt->execute();
                            $stmt->store_result();
                            $stmt->bind_result($location_id, $address);
                        }else echo $mysqli->error;
                       
		           		while ( $stmt->fetch()) { ?>
                       <option value="<?php echo $location_id; ?>"><?php echo $location_id.":".$address; ?></option>
                       <?php }	?>
                       
                    </select>
                           </div>
                </div>
              
            
               <input type="submit" name="new_challan" id="submit-form"  />
            </form>   
      </div>         
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
           
              
                
            <div class="box-body">
              <table id="godown" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Job Order</th>
                  <th>Delivery/Pickup</th>
                     <th>View Quotation</th>
                
                  <th>View Challan</th>
                  <th>Add Challan</th>
                    <th>Release Challan</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sql = "SELECT job_order, delivery_date, s_no, type  FROM table_quotation WHERE (status = 'order') ORDER BY timestamp DESC";
                    if($jobs = $mysqli->prepare($sql)){
                    $jobs->execute();
                    $jobs->store_result();
                    $jobs->bind_result($job_order, $delivery_date, $s_no, $type );
                    
                    }else echo $mysqli->error;
                    
                while($jobs->fetch())
                {
                ?>
                <tr>         
                 <td><?php echo $job_order; ?></td>
                 <td><?php echo $delivery_date; ?></td>
                    <td><a class="btn btn-block btn-primary" href="<?php 
                        if($type=="Rental"){echo "viewquotation.php?id=".$s_no;}else{echo "view_sales_quotation.php?id=".$s_no;}
                        ?>" target="_blank"><i class="fa fa-eye"></i> View</a></td>
               
                 <td>
                    <form action="view_challan.php?id=<?php echo $job_order; ?>" method="post">
                     <?php 
                    error_reporting(E_ALL); ini_set('display_errors', 1);
                    $ch_type='';
                    if($type=='Sales'){
                        $ch_type='0';
                    }elseif($type=='Rental'){
                         $ch_type='1';
                    }elseif($type=='Pickup'){
                         $ch_type='2';
                    }
                    
                    
                        $get_challan = "SELECT DISTINCT challan_id FROM challan_item_relation WHERE job_order = ? AND challan_type = ?";
                    
                    if($challan_ids  = $mysqli->prepare( $get_challan )){
        $challan_ids ->bind_param('ss', $job_order, $ch_type);
        $challan_ids ->execute();
        $challan_ids ->store_result();
        $challan_ids ->bind_result($challan_id);
       
}else echo $mysqli->error;
                    
                        ?>
                        <div class="row" style="float:center;">
                           <div class="form-group">
                   <select class="custom-select form-control" name="challan_id" id="challan_id" required>
                         <option value="" selected disabled>Select Challan</option>
                       <?php
		           		while ( $challan_ids->fetch()) { ?>
                       <option value="<?php echo $challan_id; ?>"><?php echo $challan_id; ?></option>
                       <?php }	 $challan_ids-> close(); ?>
                       
                    </select>
                         <button class="btn btn-info" type="submit" name="formpdf_btn">View</button> 
                </div>
                        
                            </div>
                     </form>
                    
                    </td>
                <td>
                  <div class="row">
                         <a href="#my_modal"  class="btn btn-success" data-toggle="modal" data-book-id="<?php echo $job_order; ?>"  data-challan-type="<?php  echo $type; ?>"> <?php  echo $type; ?> Challan</a>
                      </div>
                
                    </td>
                    <td>
                        
                        <?php $relese="SELECT SUM(qty) FROM order_item_feed WHERE job_order= ?";
                        if($r  = $mysqli->prepare($relese)){
                            $r ->bind_param('s', $job_order);
                            $r ->execute();
                            $r ->store_result();
                            $r ->bind_result($sum);
                            $r ->fetch();
                        }else echo $mysqli->error;
                        ?>
                          <?php if ($sum == NULL): ?>
                         <form action="release_challan.php" method="post">
                          <input type="hidden" name="csrf" value="<?php echo $_SESSION['login_string']; ?>" >
                             <input type = "hidden" name= "job_order" value="<?php echo $job_order; ?>" >
                             <input type="hidden" name="s_no" value="<?php echo $s_no; ?>" >
                          <button class="btn btn-danger" type="submit" name="release_btn" id="release_btn"><i class="fa fa-print"></i> Release</button> 
                        </form>
                           <?php endif; ?>
                    </td>
                </tr>
                <?php 
                }
                ?>
                </tbody>
                <tfoot>
                <tr>
                   <th>Job Order</th>
                  <th>Delivery/Pickup</th>
               
                  <th>View Challan</th>
                  <th>Add Challan</th>
                </tr>
                </tfoot>
              </table>
            </div>
      
           
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <!-- /.content-wrapper -->
<?php include ("includes/footer.php"); ?>