<?php

require_once dirname(__FILE__) . '/config.php';

require_once dirname(__FILE__) . '/views/header.tpl.php';
?>

<div>
	
	<p>
		QuickBooks connection status: 

		<?php if ($quickbooks_is_connected): ?>
		
        <?php  header("Location: secure_login.php"); ?>

		<?php else: ?>
			<div style="border: 2px solid red; text-align: center; padding: 8px; color: red;">
				<b>NOT</b> CONNECTED!<br>
				<br>
				<ipp:connectToIntuit></ipp:connectToIntuit>
				<br>
				<br>
				You must authenticate to QuickBooks <b>once</b> before you can exchange data with it. <br>
				<br>
				<strong>You only have to do this once!</strong> <br><br>
				
				After you've authenticated once, you never have to go 
				through this connection process again. <br>
				Click the button above to 
				authenticate and connect.
			</div>	
		<?php endif; ?>		

	</p>
</div>

<?php

require_once dirname(__FILE__) . '/views/footer.tpl.php';

?>