<?php include("includes/header.php"); ?>

<?php
 include("includes/dbcon.php");
include("secure/db_connect.php");
$job_order = $_GET['id'];
$challan_id = $_POST['challan_id'];
$challan_type = '';

$pickup_location_id = '';
$delivery_location_id = '';

$pickup_address = '';
$delivery_address = '';
$challan_amt = '';
$timestamp = '';

$query_loc_id = "SELECT * from table_challan WHERE challan_id = '$challan_id'";

$loc_ids = mysqli_query($con, $query_loc_id);
                  	while($locations=mysqli_fetch_array($loc_ids)){
                        
                        $pickup_location_id = $locations['pickup_loc_id'];
                        $delivery_location_id = $locations['delivery_loc_id']  ;   //JOB Order
                            $challan_type =  $locations['type'];
                        $challan_amt =  $locations['challan_tot'];
                        $timestamp = $locations['timestamp'];
                    }


    $query_pickup_loc = "SELECT * from table_location WHERE location_id='$pickup_location_id''";

$pickup_location = mysqli_query($con, $query_pickup_loc);
    while($pick=mysqli_fetch_array($pickup_location)){
                        $pickup_address = $pick['address'].'<br>'.$pick['state']."<br>".$pick['pincode'];
                    }



if($challan_type == 1) {
     $q = "SELECT  table_quotation.delivery_address, table_quotation.delivery_pincode, qb_cache_customer.billing_address, qb_cache_customer.billing_pincode FROM table_quotation , qb_cache_customer WHERE table_quotation.job_order = ? AND table_quotation.customer_id = qb_cache_customer.customer_id";
    if ( $stmt = $mysqli->prepare( $q ) ) {
        $stmt->bind_param('s', $delivery_location_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($delivery_address, $delivery_pincode, $billing_address, $billing_pincode);
        $stmt->fetch();
    }else echo $mysqli->error;
    
    $delivery_address = $delivery_address."<br>".$delivery_pincode;
    
    
     $q = "SELECT  address, state, pincode FROM table_location WHERE location_id = ? ";
    if ( $stmt = $mysqli->prepare( $q ) ) {
        $stmt->bind_param('s', $pickup_location_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($address, $state, $pincode);
        $stmt->fetch();
    }else echo $mysqli->error;
    
    $pickup_address = $address."<br>". $state."<br>". $pincode;
    
    
}
$sql = "SELECT * FROM `challan_item_relation` WHERE challan_id = '$challan_id'";
$items = mysqli_query($con, $sql);
$si=0;
$string ='';
foreach($items as $row){
   $si++;
      $string.="<tr>
            <td>$si</td>
            <td>$row[item_id]</td>
            <td>$row[item_description]</td>
            <td>$row[quantity]</td>
            <td>$row[unit_price]</td>
            <td>$row[total_price]</td>
            </tr>";
        if($row['item_type']=="Item"){
          
        }else{
           $bundle_items =  mysqli_query($con, "SELECT * from bundle_item_relation WHERE bundle_id = '".$row[item_id]."'");
            
            foreach($bundle_items as $bundle){
                $string.="<tr>
                <td></td>
                <td>$bundle[item_id]</td>
                <td></td>
                <td>$bundle[quantity]</td>
                <td></td>
                <td></td>
                </tr>";
            }
        }
}
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Challan
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Challan</a></li>
        <li class="active">View CHallan</li>
      </ol>
    </section>

    <div class="pad margin no-print">
      <div class="callout callout-info" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> Note:</h4>
        This page has been enhanced for printing. Click the print button at the bottom of the challan to print.
      </div>
    </div>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Youngman India Pvt. Ltd.
             <medium class="pull-right">
              <?php
          
          if($challan_type=='0'){
              echo "Sales Challan";
          }elseif($challan_type=='1'){
              echo "Delivery Challan";
;
          }elseif($challan_type=='2'){
              echo "Pickup Challan";
          }
          
          ?>
                 <small><strong><?php echo "<br>Dated: ".date('M j Y', strtotime($timestamp)); ?></strong></small>
              </medium>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
        <?php if($challan_type=='1') echo "<h4 style='text-align: center;'>Rental Material Not For Sale</h4>"; ?>
        <hr>
        
        <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          
         <strong> Pickup Location</strong>
          <address>
             <?php echo $pickup_address;?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        <strong>  Delivery Address</strong>
          <address>
            <?php echo $delivery_address;?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        <strong>  Billing Address</strong>
          <address>
            <?php echo $billing_address."<br>".$billing_pincode;?>
          </address>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
                <th>S. No.</th>
                <th>Item Id</th>
                <th>Description</th>
             
              <th>Qty</th>
                 <th>Unit Price</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
                <?php echo $string; ?>
              <tr>
                <th>Total</th>
                <th></th>
                <th></th>
              <th></th>
              <th></th>
                <th>₹  <?php echo  $challan_amt;?> </th>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
        
        <div class="row">
          

<table style="width:100%" class="table">
  <tr>
    <td colspan="2"><b>Company's VAT TIN:</b> 09866108354</td>
    <td>For Youngman India Pvt. Ltd.</td>
  </tr>
  <tr>
    <td colspan="2"><b>Company's Service TAX No:</b> AAACY4840MSD001</td>
    <td></td>
  </tr>
   <tr>
    <td colspan="2"><b>Company's PAN:</b> AAACY4840M</td>
    <td>Authorized signatory</td>
  </tr>
</table>
    </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="print_challan.php?id=<?php echo $_POST['challan_id']; ?>&job=<?php echo $job_order; ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
            <?php if ($_SESSION['role']=="planning"): ?>
            <form method="post" action="quash_challan.php">
            <input type="hidden" name="challan_id" value="<?php echo $challan_id; ?>">           
            <button type="submit" class="btn btn-danger pull-right" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Quash Challan
            </button>
            </form>
             <?php endif; ?>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
 <?php include("includes/footer.php"); ?>