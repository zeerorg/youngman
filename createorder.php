<?php include("includes/header.php"); ?>
<?php if($_GET['id'])
{
    
   $s_no = $_GET['id'];


    $sql = "
    SELECT
    customer_id, customer_name, delivery_address, delivery_date, delivery_pincode, total, site_name
    FROM 
    table_quotation
    WHERE
    s_no = ?";

     if($stmt = $mysqli->prepare($sql)){
        $stmt->bind_param('s', $s_no);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($customer_id, $customer_name, $delivery_address, $delivery_date, $delivery_pincode, $total, $site_name);
        $stmt->fetch();
    }else echo $mysqli->error;

    $query = "
   SELECT 
   c.credit_limit, c.security_letter, c.rental_advance, c.rental_order, c.security_check, s.outstanding
   FROM 
   `customer_local` AS c,
   qb_cache_customer AS s
   WHERE
   c.customer_id = ? AND c.customer_id = s.customer_id";
    if($q=$mysqli->prepare($query)){
        $q->bind_param('s', $customer_id);
        $q->execute();
        $q->store_result();
        $q->bind_result($credit_limit, $security_letter, $rental_advance, $rental_order, $security_check,  $outstanding);
        $q->fetch();
       // $q->close();
    }else echo $mysqli->error;

    
}else{
    echo "No id FOund";
}?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            New Order<br>

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">New Order</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <?php if(((int)$total + (int)$outstanding) > (int)$credit_limit ){ //
    
    
    $val = (int)$total . " + " . (int)$outstanding . ">" . (int)$credit_limit;
    echo $val;
    
    echo "<br>".$credit_limit;
    
    echo '<div class="box box-danger"><h3>Insufficient Credit Limit</h3></div>';
} ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <form role="form" method="post" action="example_order_add.php" enctype="multipart/form-data">
                         <input type="hidden" name="csrf" value="<?php echo $_SESSION['login_string']; ?>" >
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">


                                        <div class="form-group">
                                            <label for="id">Customer ID</label>
                                            <input type="text" class="form-control" name="id" id="id" value="<?php echo $customer_id; ?>" readonly required>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name" id="name" value="<?php echo $customer_name; ?>" readonly required>
                                        </div>
                                        <div class="form-group">
                                            <label for="po_no">PO No</label>
                                            <input type="text" class="form-control" name="po_no" id="po_no" placeholder="PO No" required>
                                        </div>


                                        <input type="hidden" class="form-control" name="description" id="description" value="Description">

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="delivery_address">Delivery Address</label>

                                            <input type="text" class="form-control" name="delivery_address" id="delivery_address" value=" <?php echo $delivery_address;?>" readonly required>

                                        </div>
                                        <div class="form-group">
                                            <label for="delivery_address_pin">Delivery Pin</label>

                                            <input type="text" class="form-control" name="delivery_address_pin" id="delivery_address_pin" value="<?php echo $delivery_pincode;?> " readonly required>

                                        </div>
                                        <div class="form-group">
                                            <label for="site_name">Site Name</label>

                                            <input type="text" class="form-control" name="site_name" id="site_name" value="<?php echo $site_name;?> " readonly required>

                                        </div>

                                        <input type="hidden" class="form-control" name="phone" id="phone" value="0">
                                        <input type="hidden" class="form-control" name="email" id="email" value="example@email.com">
                                        <div class="form-group">
                                            <label for="name">Delivery Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="del_date" class="form-control pull-right" id="datepicker" value=" <?php echo $delivery_date;?>" readonly required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <input type="hidden" value="<?php echo $s_no; ?>" name="quot">

                                <div class="row">
                                    <div class="form-group <?php if($rental_order) echo " has-warning "?> col-md-3">
                                        <label for="image1">Rental Order Image</label>
                                        <input type="file" name="fileToUpload[]" id="fileToUpload" <?php if($rental_order) echo "required"?>>
                                        <p class="help-block">Insert the item picture here</p>
                                    </div>
                                    <div class="form-group <?php if($security_letter) echo " has-warning "?> col-md-3">
                                        <label for="image1">Security Letter Image</label>
                                        <input type="file" name="fileToUpload[]" id="fileToUpload" <?php if($security_letter) echo "required"?>>
                                        <p class="help-block">Insert the item picture here</p>
                                    </div>
                                    <div class="form-group <?php if($rental_advance) echo " has-warning "?> col-md-3">
                                        <label for="image1">Rental Payment Image</label>
                                        <input type="file" name="fileToUpload[]" id="fileToUpload" <?php if($rental_advance) echo "required"?> >
                                        <p class="help-block">Insert the item picture here</p>
                                    </div>
                                    <div class="form-group <?php if($security_check) echo " has-warning "?> col-md-3">
                                        <label for="image1">Security Cheque</label>
                                        <input type="file" name="fileToUpload[]" id="fileToUpload" <?php if($security_check) echo "required"?> >
                                        <p class="help-block">Insert the item picture here</p>
                                    </div>
                                </div>
                                <hr>


                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">

                                <?php if( !(((int)$total + (int)$outstanding ) > (int)$credit_limit)) { //
    echo '<button type="submit" name="submit" id="submit" class="btn btn-primary" >
                Submit</button>';
} ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include("includes/footer.php"); ?>