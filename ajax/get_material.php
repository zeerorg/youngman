<?php
include("../secure/db_connect.php");
include("../secure/functions.php");
sec_session_start();
error_reporting(E_ALL); ini_set('display_errors', 1);
if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'  ) && login_check($mysqli) )
{
$loc_id = $_POST['location'];


$result = "
              <table class='table table-hover'>
                <tbody>
                <tr>
                  <th>Item Code</th>
                  <th>Description</th>
                  <th>Quantity</th>
                  <th>Damaged</th>
                     <th>Missing</th>
                </tr>           
";


 $query = "SELECT location_item_relation.item_id, table_item.name, location_item_relation.quantity, location_item_relation.damaged, location_item_relation.missing FROM location_item_relation, table_item WHERE location_item_relation.location_id= ? AND location_item_relation.item_id = table_item.item_code";
                                                
                                                if($Items  = $mysqli->prepare( $query )){
                                                    $Items->bind_param('s',$loc_id);
                                                    $Items ->execute();
                                                    $Items ->store_result();
                                                    $Items ->bind_result($item_id, $name, $quantity, $damaged, $missing );   

                                                    }else echo $mysqli->error;

                                                while( $Items->fetch()){
                                                    
                                                    //echo $item_id;
                                                    
                                                    $result .= "
                                                    <tr>
                  <td>".$item_id."</td>
                  <td>".$name."</td>
                  <td>".$quantity."</td>
                  <td>".$damaged."</td>
                     <td>".$missing."</td>
                </tr>
                                                    ";
                                                }

 $result .= "</tbody>
                  </table>";
echo $result;
} else echo "Not an ajax request";

?>